# Peg Solitaire Game #

This is an implementation of the game Peg Solitaire that was used on a Career Day presentation for elementary schoolers.
The idea was to show how coding can help you model and tackle real life problems.
Originally bugged by the challenge presented by the [Cracker Barrel game](https://shop.crackerbarrel.com/toys-games/games/travel-games/peg-game/606154), I decided to create a program that find a solution to the puzzle using brute force.  


### How to play it? ###

* Clone this repository
* Open the file index.html in any browser
* Or simply visit papel.games/pegsolitaire

### Coming on a next release ###

* Improve performance for solve function
* Add animations to peg moves
* Show coordinates for the board holes
* Show possible jumps lines
