//http://recmath.org/pegsolitaire/tindex.html

var selected = null;
var currentBoard = null;
var validMoves = [{
    x: 0,
    y: 2
  },
  {
    x: 0,
    y: -2
  },
  {
    x: 2,
    y: 0
  },
  {
    x: -2,
    y: 0
  },
  {
    x: 2,
    y: 2
  },
  {
    x: -2,
    y: -2
  }
];

const MODE_PLAY = 'PLAY';
const MODE_DEMO = 'DEMO'

var mode = null;

window.onload = function() {
  reset();
  loadSounds();
};

function clickHole(event) {
  var targetHole = currentBoard[event.target.id];

  if (!targetHole) {
    return;
  }

  if (targetHole.pegged) {
    // Selecting a peg
    selected = targetHole;
    selectSFX.play();
  } else {
    if (selected) {
      // Trying to move a peg
      var jumped = tryToMove(currentBoard, selected.x, selected.y, targetHole.x, targetHole.y);
      if (jumped) {
        moveSFX.play();
        selected = null;
      } else {
        // maybe play a sound?
      }
    } else {
      // Just clicked on an empty hole
    }
  }

  paint(currentBoard);

  // Check for end of game
  if (mode == MODE_PLAY) {
    if (countPegs(currentBoard) == 1) {
      setTimeout(function() {
        setClass(document.getElementById('youmadeit'), '');
      }, 1000);
    } else {
      if (whatAreThePossibleMoves(currentBoard, true).length < 1) {
        setTimeout(function() {
          setClass(document.getElementById('nomoremoves'), '');
        }, 1000);
      }
    }
  }

}

function tryToMove(board, fromX, fromY, toX, toY) {
  var from = getHole(board, fromX, fromY);
  var to = getHole(board, toX, toY);
  if (!from || !to) {
    return null;
  }
  if (!from.pegged || to.pegged) {
    return null;
  }
  var deltaX = to.x - from.x;
  var deltaY = to.y - from.y;
  var validMove = null;
  for (var move of validMoves) {
    if (move.x == deltaX && move.y == deltaY) {
      validMove = move;
    }
  }
  if (validMove) {
    var jumped = getHole(board, from.x + validMove.x / 2, from.y + validMove.y / 2);
    if (jumped.pegged) {
      from.pegged = false;
      to.pegged = true;
      jumped.pegged = false;
      return jumped;
    }
  }
  return null;
}

function paint(board) {

  if (mode === MODE_PLAY) {
    selectedColor = 'red';
    regulardColor = 'darkred';
  } else {
    selectedColor = 'lime';
    regulardColor = 'darkgreen';
  }

  for (var hole in board) {
    if (board.hasOwnProperty(hole)) {
      if (board[hole].pegged) {
        if (selected == board[hole]) {
          document.getElementById(hole).style.fill = selectedColor;
        } else {
          document.getElementById(hole).style.fill = regulardColor;
        }
      } else {
        document.getElementById(hole).style.fill = 'black';
      }
    }
  }
}

function getHole(board, x, y) {
  return board['h' + x + 'x' + y];
}

function countPegs(board) {
  var pegs = 0;
  for (var hole in board) {
    if (board.hasOwnProperty(hole)) {
      if (board[hole].pegged) {
        pegs = pegs + 1;
      }
    }
  }
  return pegs;
}

function addMove(previousMoves, move) {
  var newMoves = [].concat(previousMoves);
  newMoves.push('h' + move.fromX + 'x' + move.fromY + '');
  newMoves.push('h' + move.toX + 'x' + move.toY + '');
  return newMoves;
}

function stepByStep(clicks) {
  if (!clicks) {
    return;
  }
  var index = 0;
  var max = clicks.length;
  delay = 1000; //milliseconds

  function fakeClick() {
    var fakeEvent = {
      target: {
        id: clicks[index]
      }
    }
    clickHole(fakeEvent);
    if (++index < max && mode === MODE_DEMO)
      setTimeout(fakeClick, delay);
  }
  setTimeout(fakeClick, delay);
}

function reset() {
  mode = MODE_PLAY;

  show(document.getElementById('resetBtn'));

  if (countPegs(currentBoard) == 14) {
    // Change the current empty hole
    var found = false;
    var reset = false;
    for (var holeId in currentBoard) {
      if (currentBoard.hasOwnProperty(holeId)) {
        var hole = currentBoard[holeId];
        if (found) {
          hole.pegged = false;
          reset = true;
          break;
        }
        if (!hole.pegged) {
          hole.pegged = true;
          found = true;
        }
      }
    }
    if (!reset) {
      currentBoard['h0x0'].pegged = false;
    }

  } else {
    // Reset to classic board
    currentBoard = {
      h0x0: {
        x: 0,
        y: 0,
        pegged: false
      },
      h0x1: {
        x: 0,
        y: 1,
        pegged: true
      },
      h1x1: {
        x: 1,
        y: 1,
        pegged: true
      },
      h0x2: {
        x: 0,
        y: 2,
        pegged: true
      },
      h1x2: {
        x: 1,
        y: 2,
        pegged: true
      },
      h2x2: {
        x: 2,
        y: 2,
        pegged: true
      },
      h0x3: {
        x: 0,
        y: 3,
        pegged: true
      },
      h1x3: {
        x: 1,
        y: 3,
        pegged: true
      },
      h2x3: {
        x: 2,
        y: 3,
        pegged: true
      },
      h3x3: {
        x: 3,
        y: 3,
        pegged: true
      },
      h0x4: {
        x: 0,
        y: 4,
        pegged: true
      },
      h1x4: {
        x: 1,
        y: 4,
        pegged: true
      },
      h2x4: {
        x: 2,
        y: 4,
        pegged: true
      },
      h3x4: {
        x: 3,
        y: 4,
        pegged: true
      },
      h4x4: {
        x: 4,
        y: 4,
        pegged: true
      }
    };
  }

  paint(currentBoard);
  resetButtons();
}

function whatAreThePossibleMoves(board, returnOnFirst) {
  var possibleMoves = [];
  for (var hole in board) {
    if (board.hasOwnProperty(hole)) {
      var from = board[hole];
      if (from.pegged) {
        for (var move of validMoves) {
          var newBoard = JSON.parse(JSON.stringify(board));
          var to = {
            x: from.x + move.x,
            y: from.y + move.y
          }
          var jumped = tryToMove(newBoard, from.x, from.y, to.x, to.y);
          if (jumped) {
            possibleMoves.push({
              fromX: from.x,
              fromY: from.y,
              toX: to.x,
              toY: to.y,
              jumpedX: jumped.x,
              jumpedY: jumped.y
            });
            if (returnOnFirst) {
              return possibleMoves;
            }
          }
        }
      }
    }
  }
  return possibleMoves;
}

function explore(board, previousMoves) {
  var pegs = countPegs(board);
  if (pegs > 1) {
    var moves = whatAreThePossibleMoves(board);
    if (!moves) {
      return null;
    } else {
      for (var move of moves) {
        // Move and re-explore
        var newBoard = JSON.parse(JSON.stringify(board));
        applyMove(newBoard, move);
        var newMoves = addMove(previousMoves, move);
        var result = explore(newBoard, newMoves);
        if (result) {
          return result;
        }
      }
    }
  } else {
    // Found a solution!
    return previousMoves;
  }
  return null;

}

function applyMove(board, move) {

  var from = getHole(board, move.fromX, move.fromY);
  var to = getHole(board, move.toX, move.toY);
  var jumped = getHole(board, move.jumpedX, move.jumpedY);

  from.pegged = false;
  to.pegged = true;
  jumped.pegged = false;

}

function solve() {
  var solution = explore(currentBoard, []);
  // console.log('Solution:', solution);
  if (solution) {
    mode = MODE_DEMO;
    stepByStep(solution);
  } else {
    alert('There is no possible solution from here. You\'d better restart it.');
  }
  resetButtons();
}

function resetButtons() {
  setTimeout(function() {
    clear(document.getElementById('resetBtn'));
    clear(document.getElementById('solveBtn'));
  }, 200);
}

function loadSounds() {

  function Channel(audio_uri, volume) {
    this.audio_uri = audio_uri;
    this.resource = new Audio(audio_uri);
    this.resource.volume = volume;
  }

  function Switcher(audio_uri, num, volume) {
    this.channels = [];
    this.num = num;
    this.index = 0;
    for (var i = 0; i < num; i++) {
      this.channels.push(new Channel(audio_uri, volume));
    }
  }

  Channel.prototype.play = function() {
    // Try refreshing the resource altogether
    this.resource.load();
    this.resource.play();
  }

  Switcher.prototype.play = function() {
    this.channels[this.index++].play();
    this.index = this.index < this.num ? this.index : 0;
  }

  selectSFX = new Switcher('select.mp3', 1, 1.0);

  moveSFX = new Switcher('move.mp3', 1, 0.1);
}

function show(element) {
  setClass(element, element.id);
}

function clear(element) {
  setClass(element, 'transparent');
}

function setClass(element, className) {
  element.setAttribute("class", className);
}

function hide(element) {
  setClass(element, 'hide');
}

function nextTutorial(current){
  hide(current);
  show(document.getElementById('tutorial2'));
}
